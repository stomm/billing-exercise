package de.meshcloud.billingsample.os.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

/**
 * Event represents the state of an object in an OpenStack service
 * at a point in time when something of interest has occurred
 */
@JsonIgnoreProperties(ignoreUnknown=true)
public class Event {

	@JsonProperty("event_type")
	private String eventType;
    @JsonProperty("generated")
    private String generated;
    @JsonProperty("message_id")
    private String messageId;
    @JsonProperty("traits")
    private List<Trait> traits;

    public String getEventType() {
        return eventType;
    }

    public String getGenerated() {
        return generated;
    }

    public String getMessageId() {
        return messageId;
    }

    public List<Trait> getTraits() {
        return traits;
    }

    public void setEventType(String eventType) {
        this.eventType = eventType;
    }

    public void setGenerated(String generated) {
        this.generated = generated;
    }

    public void setMessageId(String messageId) {
        this.messageId = messageId;
    }

    public void setTraits(List<Trait> traits) {
        this.traits = traits;
    }

}
