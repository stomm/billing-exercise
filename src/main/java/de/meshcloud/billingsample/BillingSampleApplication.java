package de.meshcloud.billingsample;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BillingSampleApplication {

	public static void main(String[] args) {
		SpringApplication.run(BillingSampleApplication.class, args);
	}
}
